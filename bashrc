#!/bin/bash
stty -ixon
HISTSIZE=
HISTFILESIZE=

alias ls="ls -hN --color=auto --group-directories-first"
alias grep="grep --color=auto"
alias diff="diff --color=auto"

export EDITOR="vim"

