all: vim vim-plug bash git zsh

vim: 
	ln -sf `realpath -s ./vimrc` ${HOME}/.vimrc
	mkdir -p ${HOME}/.vim
	ln -sf `realpath -s ./UltiSnips` ${HOME}/.vim

vim-plug: vim
	vim -c ":PlugInstall --sync | qa"

bash:
	ln -sf `realpath -s ./bashrc` ${HOME}/.bashrc
	ln -sf `realpath -s ./bash_profile` ${HOME}/.bash_profile

git:
	ln -sf `realpath -s ./gitconfig` ${HOME}/.gitconfig

zsh:
	ln -sf `realpath -s ./zshrc` ${HOME}/.zshrc
	ln -sf `realpath -s ./cenouro.zsh-theme` ${HOME}/.cenouro.zsh-theme

