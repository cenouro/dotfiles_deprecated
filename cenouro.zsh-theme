#!/bin/bin/zsh
# vim: foldmethod=marker :
setopt PROMPT_SUBST

# Variables. {{{
git_branch=
git_status=

start_color=$'\e[1;2;35m'
sep_color=$'\e[1;2;90m'
vim_on=$'\e[1;2;32m'
vim_off=$'\e[1;2;31m'

u_not_repo=$'\e[1;90m'
u_staged=$'\e[1;2;33m'
u_all_unstaged=$'\e[1;2;31m'
u_clean=$'\e[1;2;32m'

uu_not_repo=$u_not_repo
uu_staged=$u_staged
uu_all_unstaged=$u_all_unstaged
uu_clean=$u_clean
# }}}

function git_u(){
  if [[ -z ${git_branch} ]]; then                       # Not a Git repo.
    echo "%{$u_not_repo%}∪"
  elif echo $git_status | grep -E "^[DRM]" -q; then     # Staged operations (some or all).
    echo "%{$u_staged%}∪"
  elif echo $git_status | grep -E "^[A?]" -v -q; then   # Unstaged operations.
    echo "%{$u_all_unstaged%}∪"
  else
    echo "%{$u_clean%}∪"
  fi
};

function git_uu(){
  if [[ -z ${git_branch} ]]; then                       # Not a Git repo.
    echo "%{$uu_not_repo%}⋓"
  elif echo $git_status | grep -E "^A" -q; then         # Staged operations.
    echo "%{$uu_staged%}⋓"
  elif echo $git_status | grep -E "^\?\?" -q; then      # All untracked files are unstaged.
    echo "%{$uu_all_unstaged%}⋓"
  else
    echo "%{$uu_clean%}⋓"
  fi
};

function set_git_vars(){
  git_branch="$(git symbolic-ref --short HEAD 2> /dev/null)"
  [ -z ${git_branch} ] && git_status= || git_status=$(git status --porcelain)
};

function vim_running(){ jobs | grep vim | grep -E "(suspended|running)" -q }

function vim_prompt(){
  local _color=
  $(vim_running) && _color=${vim_on} || _color=${vim_off}
  echo "%{$_color%}ᜡ "
};

precmd_functions=(set_git_vars)

PS1=
PS1+=$'%{$start_color%}┌─∷ \$(vim_prompt) %{$sep_color%}∷ \$(git_u) \$(git_uu) %{$sep_color%}∷ %n@jade${prompt_newline}'
PS1+=$'%{$start_color%}└─∷ %{\e[1;2;34m%}%~ %{$start_color%}$ %{\e[0m%}' 

export PS1

