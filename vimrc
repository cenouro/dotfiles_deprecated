set nocompatible
" vim: foldmethod=marker:filetype=vim:fdl=1:list :
let mapleader = ","

" vim-plug. {{{1
" Autoload vim-plug. {{{2
" NOTE: running vim with -u flag will NOT set the $MYVIMRC variable.
" NOTE: it's probably best to restart vim after installing vim-plug.
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
" }}}2

call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'morhetz/gruvbox'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --bin' }
Plug 'junegunn/fzf.vim'
Plug 'SirVer/ultisnips'
call plug#end()
" }}}1

" Options. {{{1
set rnu nu
set scrolloff=2

" Ignore useless (for Vim) files.
set wildignore+=*.o,*~,*.pyc,*.gz,*.lock,*.zip,*.xlsx

set ignorecase smartcase magic hlsearch

" Convention is 80. For better :set list, 'reserve' one char for <CR>.
set textwidth=79

" Statusline {{{2
set statusline=
set statusline+=\ ‹‹\ %t%y%h%m%r\ ››\ %F
set statusline+=%=
set statusline+=‹‹\ Line\ %2.l\ /\ %2.L\ ::\ Col\ %2.c\ ::\ %P\ ››\ 
" }}}2

" Very stable options (AKA won't change). {{{2
set shiftwidth=2 tabstop=2 expandtab
set linebreak wrap

" A buffer becomes hidden when it is abandoned.
set hidden
set showmatch
set nobackup noswapfile

set encoding=utf8

" No annoying sound on errors.
set noerrorbells novisualbell t_vb= tm=500
" }}}2
" }}}1

" Color scheme. {{{2
set background=dark
let g:gruvbox_contrast_dark='hard'
silent! colorscheme gruvbox

" It seems gruvbox does not set spellcheck colors by default. This has to
" be set after the color scheme is set.
hi SpellBad ctermfg=DarkGreen ctermbg=DarkRed cterm=bold
hi SpellCap ctermfg=Black ctermbg=DarkCyan
" }}}2

" Mappings. {{{1
" Better mappings for 'diff mode'.
vnoremap <leader>dp :diffput<bar>diffupdate<cr>
vnoremap <leader>do :diffget<bar>diffupdate<cr>
nnoremap <leader>dp :.diffput<bar>diffupdate<cr>
nnoremap <leader>do :.diffget<bar>diffupdate<cr>

" Either scroll page or jump to specific line number.
nnoremap <expr> k v:count == 0 ? '<C-y>' : 'k'
nnoremap <expr> j v:count == 0 ? '<C-e>' : 'j'

nnoremap <leader><leader> <C-w><C-w>
nnoremap <leader>o <C-w>o

" Tag mappings.
nnoremap <tab> <c-]>
nnoremap <s-tab> <c-t>

nnoremap <leader>= mmgg=G`m

" Plugins mappings. {{{2
nnoremap <leader>f :FzfFiles<cr>
nnoremap <leader>b :FzfBuffers<cr>
" TODO: <c-s> will block Vim; 'stty -ixon' must be called on shell rc.
let g:fzf_action = {
  \ 'ctrl-s': 'split',
  \ 'ctrl-v': 'vsplit' }

let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"
" }}}2
" }}}1

" Plugins configurations. {{{2
let g:fzf_command_prefix = 'Fzf'

" Customize fzf colors to match your color scheme. {{{3
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }
" }}}3

" - down / up / left / right
let g:fzf_layout = { 'down': '~15%' }

let g:UltiSnipsEditSplit="vertical"
let g:UltiSnipsSnippetDirectories=[$HOME.'/.vim/UltiSnips']
" }}}2

